import torch.nn as nn
import torch
import torchvision
import numpy as np
import matplotlib.pyplot as plt


def get_focal(key_points_output,key_point_gt,beta=4,alpha=2):

	pos = key_point_gt.eq(1).float()
	neg = key_point_gt.lt(1).float()

	neg_weights = torch.pow(1 - key_point_gt, beta)

	loss = 0

	positive_loss = torch.log(key_points_output) * torch.pow(1 - key_points_output, 2) * pos
	negative_loss = torch.log(1 - key_points_output) * torch.pow(key_points_output, 2) * neg_weights * neg

	num_pos  = pos.float().sum()

	positive_loss = positive_loss.sum()
	negative_loss = negative_loss.sum()

	if num_pos == 0:
		loss = loss - negative_loss
	else:
		loss = loss - (positive_loss + negative_loss) / num_pos

	return loss

def get_l1_loss(wh_output,wh_gt,key_point_gt):

	l1  = torch.nn.L1Loss(reduction='sum')

	mask,indices = key_point_gt.max(1)
	mask = torch.cat([mask,mask])

	wh_loss = l1(wh_output * mask, wh_gt * mask)

	return wh_loss

# def get_offset_loss(off_set,key_points_output):
		
# 	l1  = torch.nn.L1Loss(reduction='sum')

# 	mask,indices = key_points_output.max(1)
# 	indices  = (mask==1).nonzero()

# 	x = mask[indices[0][0],indices[0][1]]
# 	y = mask[indices[1][0],indices[1][1]]

# 	xy = torch.stack([x,y])
# 	xy = torch.unsqueeze(xy)

# 	off_loss = l1(off_set[:,0],xy)

# 	return off_loss