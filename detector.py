import torch.nn as nn
import torch
import torch.optim as optim
import torchvision
import numpy as np
import matplotlib.pyplot as plt

import model 
from dataloaders import *
from torch.utils.data import DataLoader
from losses import *



def det_loss(outputs,wh_gt,key_point_gt,binary_mask,off_set,lamda_size=0.1,lambda_off=1):
	key_points_output = outputs['center_point']
	wh_output = outputs['scale']

	focal_loss = get_focal(key_points_output,key_point_gt)
	l1_loss = get_l1_loss(wh_output,wh_gt,key_point_gt)

	#off_set_loss = get_offset_loss(off_set,key_points_output)
	loss = focal_loss + lamda_size * l1_loss #+ lambda_off * off_set_loss

	return loss

def run(dataloader,optimizer,model):
	for i,(img,wh_map,key_point_map,binary_mask,off_set) in enumerate(dataloader):
		outputs = model(img)
		optimizer.zero_grad()
		loss = det_loss(outputs,wh_map,key_point_map,binary_mask,off_set)
		loss.backward()
		optimizer.step()

		if i%10==0:
			print("Loss: ",loss)


if __name__ == '__main__':

	num_epochs = 10

	model = model.get_pose_net(cfg,False)
	dummy_dataset = data_set()
	dummy_dataloader = DataLoader(dummy_dataset,batch_size=1)

	optimizer = optim.Adam(model.parameters(), lr=1e-3)

	for i in range(num_epochs):
		run(dummy_dataloader,optimizer,model)

	torch.save(model,'model.pt')