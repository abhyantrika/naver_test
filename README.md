The file model.py has the definition of pose-resnet model which I took from Microsoft's Pose estimation baselines repo 
and modified it for the current problem. 

Since I do not have access to GPU currently, I have only implemented the core algorithms presented in the paper.

The dataloaders.py file contains a basic template on building a dataloader for this problem, which includes converting the bounding box into a gaussian heatmap around the center and also a heatmap for the scale regression.

The cfg.py file has all the basic configuration required to run the model. 

losses.py has all the definitions of losses used (Focal loss, l1 loss)

Finally, the detector.py model has the standard pytorch template of defining the model and running it for the desired number of epochs.
