import torch.nn as nn
import  torch
import torchvision
import cv2
import numpy as np
import glob

from PIL import Image 
import cfg
from scipy.stats import multivariate_normal 

def draw_gaussian(key_map,means,cov=3,):
	w,h = key_map.shape	
	pos = np.dstack(np.mgrid[0:w:1, 0:h:1])
	rv = multivariate_normal(mean=means,cov=cov)
	heat_map = rv.pdf(pos)

	return heat_map


# def draw_gaussian(key_map,means,cov):

# 	#Need to make sure the gaussian mean value is 1. 

# 	w,h = key_map.shape	
# 	#128 is image size
# 	x, y = np.mgrid[0:1.0:complex(w), 0:1.0:complex(h)] #128 is input size.
# 	xy = np.column_stack([x.flat, y.flat])
# 	mu = np.array(means)
# 	sigma = np.array([cov,cov])
# 	covariance = np.diag(sigma**2) 
# 	z = multivariate_normal.pdf(xy, mean=mu, cov=covariance) 
# 	z = z.reshape(x.shape) 

# 	z = z / z.max()
# 	z  = z.astype(np.float32)

# 	#mask = torch.from_numpy(z)
# 	mask = z 

# 	return mask

class data_set(object):
	"""docstring for Loader"""
	def __init__(self,folder='data/'):
		super(data_set, self).__init__()
		self.folder = folder
		self.list = glob.glob(folder+'/*.jpg')
		self.transform = torchvision.transforms.ToTensor()
		self.input_size = cfg.input_size
		self.output_size = cfg.input_size // 4

	def __len__(self):
		return len(self.list)

	def __getitem__(self,idx):
		#img = cv2.imread(self.list[0])
		img = Image.open(self.list[idx])
		h,w = img.size

		img = img.resize( (cfg.input_size,cfg.input_size))

		img = self.transform(img)

		#x_center,y_center,w,h
		gts = open(self.list[idx].replace('.jpg','.txt')).readlines()
		num_objects = len(gts)

		key_point_map = np.zeros((cfg.num_classes,self.output_size,self.output_size), dtype=np.float32)

		wh = np.zeros((2,self.output_size,self.output_size), dtype=np.float32)		

		binary_mask = np.zeros((self.output_size,self.output_size))
		off_set = np.zeros((num_objects,2),dtype=np.float32)

		for i in range(num_objects):
			label = gts[i].strip().split()
			label = list(map(float,label))

			box,cls = label[:4],label[-1]
			cls = int(cls)

			#Fit box to input_size
			box = [box[0]* (cfg.input_size/w), box[1]* (cfg.input_size/h),box[2]*(cfg.input_size/w),\
			box[3]*(cfg.input_size/h)]

			#Downsize box to output stride.
			box = [x/4 for x in box]

			bbox_label = torch.tensor(box)
			centers = torch.tensor([bbox_label[0],bbox_label[1]])
			centers_int = centers.int()

			off_set[i] = centers - centers_int.float()

			#key_point_map[i][centers_int[0]][centers_int[1]] = 1
			#Need to implement gaussian instead. 
			key_point_map[cls] = draw_gaussian(key_point_map[i],means = centers_int,cov=3)
			key_point_map[cls][centers_int[0]][centers_int[1]] = 1
			binary_mask[centers_int[0]][centers_int[1]] = 1

			width = bbox_label[2]
			height = bbox_label[3]

			#Instead of binary,allow the predictions to be scattered over the point's heat_map.
			wh = key_point_map[cls]
			wh = np.stack([wh,wh])

			#index wise.
			np.putmask(wh[0],wh[0]>0,width)
			np.putmask(wh[1],wh[1]>0,height)

		key_point_map = torch.tensor(key_point_map)
		wh_map = torch.tensor(wh)
		binary_mask = torch.tensor(binary_mask)
		off_set = torch.tensor(off_set)

		return img,wh_map,key_point_map,binary_mask,off_set