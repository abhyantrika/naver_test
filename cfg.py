NUM_DECONV_LAYERS=3
NUM_DECONV_FILTERS = [256,256,256]
NUM_DECONV_KERNELS = [4,4,4]

DECONV_WITH_BIAS = True

num_classes = 10
scale_outputs = 2

input_size = 512